# Shortie Ionic (ex TuDu)

Experiment in VueJS and Ionic.

The app is simple link shortener with nice UI.
To fully work it needs a backend that will process
request to generate shorter link.

## How to run?

- `npm install`
- `npm run serve`

## Author

Ryan Weber, One Smart Space Ltd. © 2023 All rights reserved.
