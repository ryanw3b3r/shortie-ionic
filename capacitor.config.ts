import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.toodots.2dots',
  appName: '2Dots',
  webDir: 'dist',
  bundledWebRuntime: false
};

export default config;
